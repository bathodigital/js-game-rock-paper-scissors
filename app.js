// Caching the DO

let userScore = 0;
let compScore = 0;
const userScore_span = document.getElementById("user-score");
const compScore_span = document.getElementById("comp-score");
const scoreboard_div = document.querySelector(".score-board");
const result_p = document.querySelector(".result > p");
const rock_div = document.getElementById('r');
const paper_div = document.getElementById("p");
const scissors_div = document.getElementById("s");
//storing things in the cache so we can use it later, also has perfomance benifits

//Creating the functions needed for the game

//This function generates the computer choices
function getComputerChoice(){
    const choices = ['r','p','s'];
    const randomNumber =  (Math.floor(Math.random()*3));
    return choices[randomNumber];
}

//Function to Make choices easier to see
function readableChoice(letter){
    if (letter == "r") return "Rock";
    if (letter == "p") return "Paper";
    return "Scissors";
}

//this function contains the win conditions
function win(userChoice,computerChoice){
    userScore++;
    userScore_span.innerHTML=userScore;
    result_p.innerHTML = `${readableChoice(userChoice)} beats ${readableChoice(computerChoice)} You WIN!`;
}
function lose(userChoice,computerChoice){
    compScore++;
    compScore_span.innerHTML=compScore;
    result_p.innerHTML = `${readableChoice(userChoice)} is beaten by ${readableChoice(computerChoice)} You LOSE!`;
}
function draw(userChoice,computerChoice){
    console.log("DRAW");
    result_p.innerHTML = `Both Chose ${readableChoice(computerChoice)} It's a DRAW!!`;
}
getComputerChoice();
function game(userChoice){
    const computerChoice = getComputerChoice();
    switch (userChoice+computerChoice) {
        case "rs":
        case "pr":
        case "sp":
            win(userChoice,computerChoice);
            break;
        case "sr":
        case "rp":
        case "ps":
            lose(userChoice,computerChoice);
            break;
        case "ss":
        case "rr":
        case "pp":
            draw(userChoice,computerChoice);
            break;
    }
}
// Main function
function main(){
    //adding event listerners
    rock_div.addEventListener("click", function(){
        game("r"); // calling the function
    })
    paper_div.addEventListener("click", function(){
        game("p");
    })
    scissors_div.addEventListener("click", function(){
        game("s");
    })
}

//defining the main function
main();